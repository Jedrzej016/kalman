CC=gcc
Flags=-lm -Wall
mod_flag=-c -Iinc
object=obj/main.o obj/incfile.o
include=inc/incfile.h

all: ${object}
	${CC} -o prog ${object} ${Flags}
	rm obj/*.o
	./prog

obj/main.o: src/main.c ${include}
	${CC} ${mod_flag} -o obj/main.o src/main.c ${Flags}

obj/incfile.o: src/incfile.c ${include}
	${CC} ${mod_flag} -o obj/incfile.o src/incfile.c ${Flags}
